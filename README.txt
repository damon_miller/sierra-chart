This code reads the intraday data files used by Sierra Chart into memory. The files can get quite large and the 
typical access to the data records is a combination of linear and random access. So the data records are read 
into a memory mapped file which allows random access.