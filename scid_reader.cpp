//#include "sc_pointer.hpp"
#include "sierrachart.h"
#include <iterator> 

#include <boost/interprocess/file_mapping.hpp>
#include <boost/filesystem.hpp>
#include <boost/interprocess/mapped_region.hpp>


typedef UINT32 u_int32;   // Unix uses u_int32
typedef UINT16 u_int16;   // Unix uses u_int32


// Scid file format: record and header structures
// https://www.sierrachart.com/index.php?page=doc/IntradayDataFileFormat.html
struct s_IntradayRecord
{
	double DateTimeGMT;

	float Open;
	float High;
	float Low;
	float Close;

	u_int32 NumTrades;
	u_int32 TotalVolume;
	u_int32 BidVolume;
	u_int32 AskVolume;


	s_IntradayRecord()// Constructor
	{
		DateTimeGMT = 0.0;
		Open = 0.0;
		High = 0.0;
		Low = 0.0;
		Close = 0.0;
		NumTrades = 0;
		TotalVolume = 0;
		BidVolume = 0;
		AskVolume = 0;
	}
};

struct s_IntradayHeader
{
	char FileTypeUniqueHeaderID[4];  // Set to the text string: "SCID"
	u_int32 HeaderSize; // Set to the header size in bytes.

	u_int32 RecordSize; // Set to the record size in bytes.
	u_int16 Version; // Automatically set to the current version. Currently 1.
	u_int16 Unused1; // Not used.

	u_int32 UTCStartIndex;  // This should be 0.

	char Reserve[36]; // Not used.

	s_IntradayHeader()
	{
		//strncpy(FileTypeUniqueHeaderID, "SCID", 4);
		strncpy_s(FileTypeUniqueHeaderID, "SCID", 4);
		HeaderSize = sizeof(s_IntradayHeader);
		//RecordSize = sizeof(s_Record);
		RecordSize = sizeof(s_IntradayRecord);
		Version = 1;
		Unused1 = 0;
		UTCStartIndex = 0;
		memset(Reserve, 0, sizeof(Reserve));
	}
};


// iterator for bidirectional reading access to file with scid format
// http://www.cplusplus.com/reference/iterator/iterator/
class ScidBarIterator : public std::iterator<std::forward_iterator_tag, s_IntradayRecord> {

	FILE *fp;


public:
	int scidRecordIndex;
	ScidBarIterator(FILE * _fp, int _scidRecordIndex) 
		: fp(_fp), scidRecordIndex(_scidRecordIndex)
	{
		
	}

	ScidBarIterator operator ++ () {
		return ScidBarIterator(fp, ++scidRecordIndex);
	}

	s_IntradayRecord operator * () {
		s_IntradayRecord ir;

		fseek(fp, sizeof(s_IntradayHeader) + sizeof(s_IntradayRecord) * scidRecordIndex, SEEK_SET);
		// http://en.cppreference.com/w/c/io/fread
		fread(&ir, sizeof(s_IntradayRecord), 1, fp);

		return ir;
	}

	bool operator == (const ScidBarIterator& rhs) { 
		return scidRecordIndex == rhs.scidRecordIndex;
	}
	bool operator != (const ScidBarIterator& rhs) {
		return scidRecordIndex != rhs.scidRecordIndex;
	}
};


// creates unique keys for storing persistent integers
class PersistentIntCounter {

	int countIndex = 0;

public:
	PersistentIntCounter(SCStudyGraphRef sc)
	{
	}

	int getUniqueKey() {
		return countIndex++;
	}
};


// scid file reader that creates container for subsequent reads of records
// uses ScidBarIterator to find beginning index of records
// accesses records through file seeking (determined to be slower than memory mapped files)
class ScidBar {

	FILE *fp;
	int& baseRecordIndex;

	s_IntradayRecord getRecord(int i) {
		s_IntradayRecord ir;

		fseek(fp, sizeof(s_IntradayHeader) + sizeof(s_IntradayRecord) * (baseRecordIndex + i), SEEK_SET);
		// http://en.cppreference.com/w/c/io/fread
		fread(&ir, sizeof(s_IntradayRecord), 1, fp);

		return ir;
	}

public:
	int& numberOfRecordsInChart;
	int& updateStartIndex;

	ScidBar(SCStudyGraphRef sc, PersistentIntCounter& pic)
		: baseRecordIndex(sc.GetPersistentInt(pic.getUniqueKey()))
		, updateStartIndex(sc.GetPersistentInt(pic.getUniqueKey()))
		, numberOfRecordsInChart(sc.GetPersistentInt(pic.getUniqueKey()))
		
		// http://en.cppreference.com/w/c/io/fread
		//fp(fopen(sc.DataFile, "rb"))// must use binary mode
	{
		fopen_s(&fp, sc.DataFile, "rb");  // must use binary mode
		
		if (sc.IsFullRecalculation) {
			updateStartIndex = 0;

			// https://stackoverflow.com/questions/238603/how-can-i-get-a-files-size-in-c
			fseek(fp, 0L, SEEK_END);
			int fileSize = ftell(fp);
			// go to the beginning
			rewind(fp);

			int numberOfRecords = (fileSize - sizeof(s_IntradayHeader)) / sizeof(s_IntradayRecord);

			// http://en.cppreference.com/w/c/io/fseek

			SCDateTime startingDateTimeInUTC = sc.BaseDateTimeIn[0] - sc.TimeScaleAdjustment;  // time zone setting of file records 
			ScidBarIterator it = std::lower_bound(
				ScidBarIterator(fp, 0), 
				ScidBarIterator(fp, numberOfRecords),
				startingDateTimeInUTC,
				[] (s_IntradayRecord a, SCDateTime dateTime_val) {
					return SCDateTime(a.DateTimeGMT) < dateTime_val;
				}
			);

			baseRecordIndex = it.scidRecordIndex;
			numberOfRecordsInChart = numberOfRecords - baseRecordIndex;
		}
	}

	~ScidBar() {
		fclose(fp);

		updateStartIndex = -1 + numberOfRecordsInChart;
	}

	s_IntradayRecord operator [] (int i) {
		return getRecord(i);
	}
};


//	Scid file reader that creates container for subsequent reads of records
/*	
	Record access is through memory-mapped-files using boost, which is determined 
	to be faster than accessing records through file seeking
*/
class ScidBar2 {

	s_IntradayRecord* regionAddress;

	boost::interprocess::file_mapping m_file;
	boost::interprocess::mapped_region region;

	int& baseRecordIndex;

public:
	int& updateStartIndex;
	int numberOfRecordsInChart;

	ScidBar2(SCStudyInterfaceRef sc, PersistentIntCounter& pic)
		: baseRecordIndex(sc.GetPersistentInt(pic.getUniqueKey()))
		, updateStartIndex(sc.GetPersistentInt(pic.getUniqueKey()))
	{
		// http://www.boost.org/doc/libs/1_63_0/doc/html/interprocess/sharedmemorybetweenprocesses.html#interprocess.sharedmemorybetweenprocesses.mapped_file
		using namespace boost::interprocess;
		
		m_file = file_mapping 
			( sc.DataFile       //filename
			, read_only             //read-write mode
		);

		//boost::filesystem::path p(sc.DataFile.GetChars());
		//auto fileSize = boost::filesystem::file_size(p);
		//auto numberOfRecords = (fileSize - sizeof(s_IntradayHeader)) / sizeof(s_IntradayRecord);
		int maximumNumberOfRecordsUsed = sc.FileRecordIndexOfLastDataRecordInChart + 1;

		
		//boost::interprocess::mapped_region region
		region = boost::interprocess::mapped_region
			( m_file                   //Memory-mappable object
			, read_only               //Access mode
			, sizeof(s_IntradayHeader)             //Offset from the beginning of shm
		//	, (std::size_t)fileSize - sizeof(s_IntradayHeader)      //Length of the region
			, sizeof(s_IntradayRecord) * maximumNumberOfRecordsUsed      //Length of the region
		);

		//Get the address of the region
		regionAddress = (s_IntradayRecord*)region.get_address();

		//Get the size of the region
		//int value1 = region.get_size();

		if (sc.IsFullRecalculation) {
			updateStartIndex = 0;
			
			//SCDateTime startingDateTimeInUTC = sc.BaseDateTimeIn[0] - sc.TimeScaleAdjustment;  // time zone setting of file records 
			SCDateTime startingDateTimeInUTC = sc.AdjustDateTimeToGMT(sc.BaseDateTimeIn[0]);// time zone setting of file records 
			// http://www.cplusplus.com/reference/algorithm/lower_bound/
			s_IntradayRecord* it = std::lower_bound(
				regionAddress,
				regionAddress + maximumNumberOfRecordsUsed,
				startingDateTimeInUTC,
				[] (s_IntradayRecord a, SCDateTime dateTime_val) {
					return SCDateTime(a.DateTimeGMT) < dateTime_val;
				}
			);

			baseRecordIndex = it - regionAddress;
		}

		numberOfRecordsInChart = maximumNumberOfRecordsUsed - baseRecordIndex;
	}

	~ScidBar2() {
		updateStartIndex = -1 + numberOfRecordsInChart;
	}

	s_IntradayRecord operator [] (int i) {
		return regionAddress[baseRecordIndex + i];
	}
};